﻿using UnityEngine;
using System.Collections;

public class Attaker : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision) {

		if(collision.gameObject.name == "Enemy(Clone)")
		{
			if(ScoreKeeper.main != null)
			{
				ScoreKeeper.main.AddEnemyDamage();

				EnemyGard enemyGard = collision.gameObject.GetComponent<EnemyGard>();
				if(enemyGard)
				{
					enemyGard.Gard(Vector3.Lerp(this.transform.position,collision.gameObject.transform.position,0.5f));
				}

			}
		} else 
		{
			if(SoundManager.main != null)
			{
				SoundManager.main.PlayBallHit();
			}
		}
	} 
}
