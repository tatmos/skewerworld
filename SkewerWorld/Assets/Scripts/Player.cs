﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public LineRenderer lineRendererPrefab;

	LineRenderer lineRenderer;
	FixedJoint fixedJoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if(lineRenderer != null)
		{
			lineRenderer.SetPosition(0,this.transform.position);
			if(fixedJoint.connectedBody != null){
				lineRenderer.SetPosition(1,fixedJoint.connectedBody.gameObject.transform.position);
			}
		}
	}

	public void DestroyPre()
	{
		if(lineRenderer != null){
			Destroy(lineRenderer);
			lineRenderer = null;
		}
	}

	void OnCollisionEnter(Collision collision) {

		/*
		foreach (ContactPoint contact in collision.contacts) {
			Debug.DrawRay(contact.point, contact.normal, Color.white);
		}
		if (collision.relativeVelocity.magnitude > 2)
			audio.Play();
		*/ 

		//Debug.Log("col " + collision.gameObject.name);

		if(collision.gameObject.name == "Sphere(Clone)")
		{

			fixedJoint = collision.gameObject.AddComponent<FixedJoint>();
			fixedJoint.connectedBody = this.gameObject.rigidbody;
			fixedJoint.enableCollision = true;

			collision.gameObject.rigidbody.mass = 0.001f;
			collision.gameObject.name = "Sphere(Jointed)";
			Player player = collision.gameObject.AddComponent<Player>();
			player.lineRendererPrefab = lineRendererPrefab;
			player.lineRenderer = GameObject.Instantiate(lineRendererPrefab) as LineRenderer;
			player.lineRenderer.transform.parent = collision.gameObject.transform;
			player.fixedJoint = fixedJoint;
		 
			//collision.gameObject.transform.parent = this.transform;
			//collision.gameObject.rigidbody.collider.enabled = false;
			//collision.gameObject.rigidbody.AddForce(Vector3.zero);
			//collision.gameObject.transform.position = this.transform.position;

			//GameObject.Destroy(collision.gameObject,2);

			if(ScoreKeeper.main != null)
			{
				ScoreKeeper.main.AddScore();
			}
		} else if(collision.gameObject.name == "Cube"){
			if(ScoreKeeper.main != null && this.gameObject.name == "Player(Clone)")
			{
				ScoreKeeper.main.AddDamage(this.gameObject.transform.position);
			}
		}else {
			if(ScoreKeeper.main != null && this.gameObject.name == "Player(Clone)")
			{
				ScoreKeeper.main.KabeHit();
			}
		}
	} 
}
