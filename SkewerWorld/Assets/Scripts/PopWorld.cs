﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopWorld : MonoBehaviour {

	public GameObject ball;
	public GameObject enemyPrefab;
	public GameObject playerPrefab;
	public Material releaseMaterial;

	// Use this for initialization
	void Start () {	
		PopEnemy();
		PopPlayer();
	}

	float popTime = 0;

	public float popRate = 3f;

	// Update is called once per frame
	void Update () {
		PopBall();

		MoveEnemy();
		MovePlayer();

		if(Input.GetKeyDown(KeyCode.Space) || 
		   Input.GetMouseButtonDown(0) ||
		   Input.anyKey){
			BreakJoint();

			if(anyKeyEscapeFlag)
			{
				if(SoundManager.main != null)
				{
					SoundManager.main.PlayThrow();
				}
				Application.LoadLevel("title");
			}
		}

		if(//Input.GetKeyDown(KeyCode.R) || 
		   Input.GetKeyDown(KeyCode.Escape))
		{
			if(SoundManager.main != null)
			{
				SoundManager.main.PlayThrow();
			}
			Application.LoadLevel("title");
		}


		if(ScoreKeeper.main.clearTime > 0){
			if(clearFlag == false){
				clearFlag = true;
				StartCoroutine("GoTitle");
			}
		}
	}

	bool clearFlag = false;
	bool anyKeyEscapeFlag = false;
	float enemyMoveTime = 0;
	float playerMoveTime = 0;

	void OnLevelWasLoaded(int level) {
		popTime = 0;
		clearFlag = false;
		anyKeyEscapeFlag = false;
		enemyMoveTime = 0;
		playerMoveTime = 0;
	}


	IEnumerator GoTitle()
	{
		yield return new WaitForSeconds(5.5f);
		anyKeyEscapeFlag = true;
	}


	void BreakJoint()
	{
		if(ScoreKeeper.main.enemyDamage <= 0){
			return;
		}

		if(SoundManager.main != null)
		{
			SoundManager.main.PlayThrow();
		}

		//GameObject[] objectList = this.gameObject.ch

		GameObject enemy = GameObject.FindWithTag("Enemy");

		List<GameObject> removeList = new List<GameObject>();

		foreach( Transform child in transform)
		{
			GameObject childGObj = child.gameObject;

			if(childGObj.name == "Sphere(Jointed)")
			{
				Player player = childGObj.GetComponent<Player>();
				if(player != null){
					player.DestroyPre();
					Destroy(player);

					childGObj.AddComponent<Attaker>();
                }
				FixedJoint fixedJoint = childGObj.GetComponent<FixedJoint>();
				if(fixedJoint != null){
					Destroy(fixedJoint);
                }
                
                Vector3 direction = enemy.transform.position - childGObj.transform.position;
				childGObj.rigidbody.AddForceAtPosition(direction.normalized*2f, transform.position);
				//Debug.Log("dir " + direction);

				Color oldColor = childGObj.renderer.material.color;

				childGObj.renderer.material = releaseMaterial;
				childGObj.renderer.material.color 
					= new Color(oldColor.r, 
					            oldColor.g, 
					            oldColor.b, 
					            0.5f);

				TrailRenderer trailRenderer = childGObj.GetComponent<TrailRenderer>();
				if(trailRenderer == null){
					trailRenderer = childGObj.AddComponent<TrailRenderer>();
					trailRenderer.material = this.releaseMaterial;
					trailRenderer.startWidth = 0.25f;
					trailRenderer.endWidth = 0.0f;
					trailRenderer.material.color = new Color(oldColor.r, 
					                                         oldColor.g, 
					                                         oldColor.b, 
					                                         0.5f);
				}

				removeList.Add(childGObj);
			}
        }
        
		foreach( GameObject child in removeList)
        {
			Destroy(child,1.0f);
		}
    }
    
	GameObject PopOnFieldSmooth(GameObject prefab)
	{
		
		GameObject enemy = GameObject.FindWithTag("Enemy");
		
		GameObject go = Instantiate(ball,new Vector3(Random.Range(-5f,5f),Random.Range(-5f,5f),0),Quaternion.identity) as GameObject;

		Vector3 destPos = enemy.transform.position;

		Vector3 direction = destPos - go.transform.position;
		go.rigidbody.AddForceAtPosition(direction.normalized*2f, Vector3.zero);

		return go;
	}

    
    void PopBall()
	{
		if(ScoreKeeper.main != null){
			if(ScoreKeeper.main.enemyDamage <= 0){
				return;
			}
		}

		if(popTime + popRate < Time.timeSinceLevelLoad){
			GameObject go = this.PopOnFieldSmooth(ball);
			go.transform.parent = this.transform;
			
			go.renderer.material.color = new Color(Random.value, Random.value, Random.value, 1.0f);
			
			//GameObject.Destroy(go,20f);
			popTime = Time.timeSinceLevelLoad;
		}

		//if(popRate > 0.1f){
		//	popRate -= 0.01f;
		//}
	}

	GameObject enemy;
	void PopEnemy()
	{
		enemy = Instantiate(enemyPrefab,new Vector3(Random.Range(-5f,5f),Random.Range(-5f,5f),0),Quaternion.identity) as GameObject;
		enemy.transform.parent = this.transform;
	}


	void MoveEnemy()
	{
		if(ScoreKeeper.main.enemyDamage <= 0){
			return;
		}

		if(enemyMoveTime < Time.timeSinceLevelLoad){

			float moveGain = 10f;
			if(ScoreKeeper.main != null){
				//moveGain = (100-ScoreKeeper.main.enemyDamage)*0.1f;
			}

			enemy.rigidbody.AddForce(new Vector3(Random.Range(-5f,5f)*moveGain,Random.Range(-5f,5f)*moveGain,0),ForceMode.Impulse);
			enemyMoveTime = Time.timeSinceLevelLoad + 1f;

			enemy.transform.position = this.Clamp(enemy.transform.position);
		}
	}

	Vector2 Clamp (Vector2 pos)
	{
		Vector2 min = new Vector2(-9, -6);
		Vector2 max = new Vector2(9, 6);

		pos.x = Mathf.Clamp (pos.x, min.x, max.x);
		pos.y = Mathf.Clamp (pos.y, min.y, max.y);

		return pos;
	}

	GameObject player;
	void PopPlayer()
	{
		player = Instantiate(playerPrefab,new Vector3(Random.Range(-5f,5f),Random.Range(-5f,5f),0),Quaternion.identity) as GameObject;
		player.transform.parent = this.transform;
	}


	void MovePlayer()
	{
		if(ScoreKeeper.main.enemyDamage <= 0){
			return;
		}

		if(playerMoveTime + 0.2f < Time.timeSinceLevelLoad){
			player.rigidbody.AddForce(new Vector3(Random.Range(-5f,5f)*5f,Random.Range(-5f,5f)*5f,0),ForceMode.Impulse);
			playerMoveTime = Time.timeSinceLevelLoad;
		}
	}
}
