﻿using UnityEngine;
using System.Collections;

public class ClampField : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = this.Clamp(this.transform.position);
	}

	Vector2 Clamp (Vector2 pos)
	{
		Vector2 min = new Vector2(-8.5f, -4.5f);
		Vector2 max = new Vector2(8.5f, 6);
		
		pos.x = Mathf.Clamp (pos.x, min.x, max.x);
		pos.y = Mathf.Clamp (pos.y, min.y, max.y);
		
		return pos;
	}
}
