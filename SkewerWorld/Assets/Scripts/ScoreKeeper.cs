﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class ScoreKeeper : MonoBehaviour {

	public static ScoreKeeper main;
	public List<AudioClip> audioClipList = new List<AudioClip>(); 
	public List<AudioSource> audioSouceList = new List<AudioSource>();

	ParticleSystem ps;

	void Awake()
	{
		if(main == null)
		{
			main = this;
			DontDestroyOnLoad(this.gameObject);

			RestoreScores();
		} else {
			GameObject.Destroy(this.gameObject);
		}
	}
	TextMesh textMesh;

	// Use this for initialization
	void Start () {
		textMesh = GetComponentInChildren<TextMesh>();
		for(int i = 0;i<4;i++){
			audioSouceList.Add(this.gameObject.AddComponent<AudioSource>());
		}
		ps = GetComponentInChildren<ParticleSystem>();
	}

	int score = 0;
	public int enemyDamage = 100;
	public float clearTime = 0;

	void OnLevelWasLoaded(int level) {
		score = 0;
		enemyDamage = 100;
		clearTime = 0;
		clearFlag = false;
	}

	public class ScoreStruct
	{
		public ScoreStruct(int score_ ,int clearTime_)
		{
			score = score_;
			clearTime = clearTime_;
			recordTime = DateTime.Now;
		}
		public int score = 0;
		public int clearTime = 0;
		public System.DateTime recordTime;
		public string recordTimeStr = "";
	}
	public List<ScoreStruct> scoreList = new List<ScoreStruct>();

	void ScoreRanking()
	{
		foreach(ScoreStruct ss in scoreList)
		{
			textMesh.text += "\nScore:" + ss.score.ToString("D04") + " Clear Time:" + ss.clearTime.ToString("D03");
			if(ss.recordTimeStr == ""){
				ss.recordTimeStr = " - " + ss.recordTime.ToShortDateString() + " " + ss.recordTime.ToShortTimeString() + " - ";
			}
			textMesh.text += ss.recordTimeStr;
		}

		StoreScores();
	}

	void StoreScores()
	{
		PlayerPrefs.DeleteAll();

		int i = 0;
		foreach(ScoreStruct ss in scoreList)
		{
			PlayerPrefs.SetInt("score" + i.ToString("D03"), ss.score);
			PlayerPrefs.SetInt("clearTime" + i.ToString("D03"), ss.clearTime);
			PlayerPrefs.SetString("recordTimeStr" + i.ToString("D03"), ss.recordTimeStr);

			if(i > 100)return;
		}
	}

	void RestoreScores()
	{
		for(int i =0;i<100;i++){
			int score = PlayerPrefs.GetInt("score" + i.ToString("D03"), 0);
			int clearTime = PlayerPrefs.GetInt("clearTime" + i.ToString("D03"), 0);
			string recordTimeStr = PlayerPrefs.GetString("recordTimeStr" + i.ToString("D03"), "");

			if(score == 0)return;

			ScoreStruct ss = new ScoreStruct(score,clearTime);
			ss.recordTimeStr = recordTimeStr;
			scoreList.Add(ss);
		}
	}


	bool clearFlag = false;

	// Update is called once per frame
	void Update () {
		textMesh.text = "Score:" + score.ToString("D04");
		if(clearTime > 0){
			textMesh.text += " Enemy Destroyed Time:" + ((int)clearTime).ToString("D03");
			if(clearFlag == false)
			{
				clearFlag = true;
				scoreList.Add(new ScoreStruct(score,(int)clearTime));

				scoreList.Sort(delegate(ScoreStruct a,ScoreStruct b){return a.clearTime - b.clearTime;});
			}
		} else {
			textMesh.text += " Enemy:" + enemyDamage.ToString("D03");
		}
		textMesh.text += " - Skewer World -";

		if(clearFlag)
		{
			ScoreRanking();
		}
	}
		

	public void AddScore()
	{
		score++;

		audioSouceList[0].clip = audioClipList[0];
		audioSouceList[0].volume = 1.0f;
		
		audioSouceList[0].pitch = Mathf.Pow(2,UnityEngine.Random.Range(0,score%12)/12.0f);
		audioSouceList[0].Play();
	}

	public void AddDamage(Vector3 pos)
	{
		audioSouceList[1].clip = audioClipList[1];
		audioSouceList[1].volume = 0.02f;
		
		audioSouceList[1].pitch = Mathf.Pow(2,UnityEngine.Random.Range(0,12)/12.0f);
		audioSouceList[1].Play();

		ps.transform.position = pos;
		//Debug.Log("pos " + pos);
		//ps.Emit(pos,Vector3.zero,1,1,Color.red);
	}

	public void KabeHit()
	{
		audioSouceList[2].clip = audioClipList[2];
		audioSouceList[2].volume = 0.01f;

		audioSouceList[2].pitch = Mathf.Pow(2,UnityEngine.Random.Range(0,12)/12.0f);
		audioSouceList[2].Play();
	}


	public void AddEnemyDamage()
	{
		enemyDamage--;

		audioSouceList[3].clip = audioClipList[3];
		audioSouceList[3].volume = 1f;
		
		audioSouceList[3].pitch = Mathf.Pow(2,UnityEngine.Random.Range(0,12)/12.0f);
		audioSouceList[3].Play();

		if(enemyDamage == 0){
			//enemyDamage = 0;
			clearTime = Time.timeSinceLevelLoad;

			if(SoundManager.main != null)
			{
				SoundManager.main.PlayClear();
			}
		} else {
			if(SoundManager.main != null)
			{
				if(enemyDamage > 90){
					SoundManager.main.PlayBGM(0);
				} else if(enemyDamage > 60){
					SoundManager.main.PlayBGM(1);
				} else if(enemyDamage > 40){
					SoundManager.main.PlayBGM(2);
				} else if(enemyDamage > 30){
					SoundManager.main.PlayBGM(3);
				}   else if(enemyDamage > 20){
					SoundManager.main.PlayBGM(4);
				}   else if(enemyDamage > 5){
					SoundManager.main.PlayBGM(5);
				}  
			}
		}
	}

}
