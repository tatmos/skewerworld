﻿using UnityEngine;
using System.Collections;

public class EnemyGard : MonoBehaviour {

	public GameObject gardObjectPrefab;
	GameObject gardObject;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Gard(Vector3 pos)
	{
		gardObject = Instantiate(gardObjectPrefab,pos,Quaternion.identity) as GameObject;
		//gardObject.transform.parent = this.transform;
		gardObject.transform.Translate(1,0,0);

		Destroy(gardObject,2f);
	}


}
