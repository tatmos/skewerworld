﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SoundManager : MonoBehaviour {

	public static SoundManager main;
	public List<AudioClip> audioClipList = new List<AudioClip>(); 
	 List<AudioSource> audioSouceList = new List<AudioSource>();

	public List<AudioClip> audioBGMClipList = new List<AudioClip>(); 
	 List<AudioSource> audioBGMSouceList = new List<AudioSource>();


	ParticleSystem ps;
	
	void Awake()
	{
		if(main == null)
		{
			main = this;
			DontDestroyOnLoad(this.gameObject);
		} else {
			GameObject.Destroy(this.gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		for(int i = 0;i<4;i++){
			audioSouceList.Add(this.gameObject.AddComponent<AudioSource>());
		}
		for(int i = 0;i<1;i++){
			audioBGMSouceList.Add(this.gameObject.AddComponent<AudioSource>());
			audioBGMSouceList[i].loop = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayThrow()
	{
		int soundId = 0;
		audioSouceList[soundId].clip = audioClipList[soundId];
		audioSouceList[soundId].volume = 0.2f;
		
		audioSouceList[soundId].pitch = Mathf.Pow(2,Random.Range(0,12)/12.0f);
		audioSouceList[soundId].Play();
	}

	public void PlayBallHit()
	{
		int soundId = 1;
		audioSouceList[soundId].clip = audioClipList[soundId];
		audioSouceList[soundId].volume = 1.0f;
		
		audioSouceList[soundId].pitch = Mathf.Pow(2,Random.Range(0,12)/12.0f);
		audioSouceList[soundId].Play();
	}
	public void PlayClear()
	{
		int soundId = 2;
		audioSouceList[soundId].clip = audioClipList[soundId];
		audioSouceList[soundId].volume = 1.0f;
		
		//audioSouceList[soundId].pitch = Mathf.Pow(2,Random.Range(0,12)/12.0f);
		audioSouceList[soundId].Play();

		audioBGMSouceList[0].Stop();
	}
	public void PlayBGM(int level)
	{
		int soundId = 0;
		audioBGMSouceList[soundId].clip = audioBGMClipList[level%audioBGMClipList.Count];
		audioBGMSouceList[soundId].volume = 1.0f;

		float seekPos = 0;
		if(audioBGMSouceList[soundId].isPlaying)
		{
			seekPos = audioBGMSouceList[soundId].time;
		}

		audioBGMSouceList[soundId].Play();
		audioBGMSouceList[soundId].time = seekPos;
	}

}
